﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    interface ICrud
    {
        public DBReturn Insert();
        public DBReturn Delete();
        public DBReturn Update();
        public DBReturn Select();
    }
}
