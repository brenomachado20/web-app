﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DataAccessLayer
{
    public class DBConnection
    {
        public static List<DBConnection> DBConnectionSessionList { get; set; }

        public string SesssionId { get; set; }
        public DataTable DataReader { get; set; }
        public string ConnectionString { get; set; }
        public bool ConnectionOpen { get; set; }

        ~DBConnection()
        {
            DataReader = null;
            Closed();
        }

        DBConnection(string pSessionId)
        {
            DBConnection dBConnection = FindConnection(pSessionId);
            if(dBConnection != null)
            {
                if (!dBConnection.ConnectionOpen)
                {
                    dBConnection.ConnectionOpen = true;
                }
            }
        }

        private bool Open()
        {
            bool success = false;
            
            return success;
        }

        private bool Closed(string pSessionId = null)
        {
            bool success = false;
            if (!string.IsNullOrEmpty(pSessionId))
            {
                DBConnection dBConnection = FindConnection(pSessionId);

                if (dBConnection == null)
                {
                    throw new Exception("Connection session not found");
                }
                else
                {
                    RemoveConnection(dBConnection);
                }
            }
            else
            {
                RemoveConnectionAll();
            }

            return success;
        }

        private DBConnection FindConnection(string pSessionId)
        {
            return DBConnectionSessionList.FirstOrDefault(b => b.SesssionId == pSessionId);
        }

        private void RemoveConnection(DBConnection pDBConnection)
        {
            DBConnectionSessionList.Remove(pDBConnection);
        }

        private void RemoveConnectionAll()
        {
            DBConnectionSessionList = null;
        }
    }
}
