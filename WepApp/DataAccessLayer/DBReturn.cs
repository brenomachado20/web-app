﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataAccessLayer
{
    public class DBReturn
    {
        public bool Success { get; set; }
        public string UIMessage { get; set; }
        public MessageEnum UIMessageType { get; set; }
        public enum MessageEnum
        {
            [Description("Error operation")]
            M100,
            [Description("Success")]
            M200,
            [Description("Timeout")]
            M300
        }

        public OperationEnum Operation { get; set; }
        public enum OperationEnum
        {
            UNDEFINED,
            CREATE,
            READ,
            UPDATE,
            DELETE
        }

        public DBReturn()
        {
            Operation = OperationEnum.UNDEFINED;
        }

        public void LoadOperation(MessageEnum pMessage)
        {
            UIMessage = LoadMessage(pMessage);
        }

        public void LoadSuccess()
        {
            UIMessage = LoadMessage(MessageEnum.M200);
        }

        public void LoadFaild(MessageEnum pMessage)
        {
            UIMessage = LoadMessage(pMessage);
        }

        private string LoadMessage(MessageEnum pMessage)
        {
            string message = null;
            switch (pMessage)
            {
                case MessageEnum.M100:
                    message = $"Error: {pMessage}, contact Support.";
                    break;
                case MessageEnum.M200:
                    message = $"Success.";
                    break;
                case MessageEnum.M300:
                    message = $"Error: {pMessage}, contact Support.";
                    break;
            }

            if (!string.IsNullOrEmpty(message))
            {
                throw new Exception("message cannot be undefined");
            }

            return message;
        }
    }
}
