﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer
{
    public class DBLog
    {
        public string Sql { get; set; }
        public DBReturn DBReturn { get; set; }
    }
}
