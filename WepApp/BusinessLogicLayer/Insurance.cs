﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class Insurance
    {
        public Customer Customer { get; set; }
        public Car Car { get; set; }
    }
}
