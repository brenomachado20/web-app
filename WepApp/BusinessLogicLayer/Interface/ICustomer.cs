﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer.Interface
{
    public interface ICustomer
    {
        bool Payment();

        bool Cancel();

        bool Disapprove();

        bool Accept();
    }
}
