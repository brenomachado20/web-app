﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class InsuranceOrder
    {
        public Insurance Insurance { get; set; }
        public Payment Payment { get; set; }
        public InsuranceOrderState State { get; set; }
        public enum InsuranceOrderState
        {
            Payment,
            Canceled,
            WaitingPayment
        }
    }
}
