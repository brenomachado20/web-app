﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogicLayer
{
    public class Payment
    {
        public double Price { get; set; }
        public MethodEnum Method { get; set; }

        public enum MethodEnum
        {
            Money,
            DebitCard,
            CreditCard
        }
    }
}
