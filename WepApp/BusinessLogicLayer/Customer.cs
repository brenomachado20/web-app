﻿using BusinessLogicLayer.Interface;
using System;

namespace BusinessLogicLayer
{
    public class Customer : ICustomer
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }

        public Customer(Customer pCustomers)
        {
            Id = pCustomers.Id;
            Name = pCustomers.Name;
            Age = pCustomers.Age;
        }

        public bool Payment()
        {
            throw new NotImplementedException();
        }

        public bool Cancel()
        {
            throw new NotImplementedException();
        }

        public bool Disapprove()
        {
            throw new NotImplementedException();
        }

        public bool Accept()
        {
            throw new NotImplementedException();
        }
    }
}
